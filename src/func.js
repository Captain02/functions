const getSum = (str1, str2) => {
  if(isNaN(str1) || isNaN(str2) || typeof str1 != 'string' || typeof str2 != 'string')
    return false;

  var num1 = str1 == ''? 0 : parseFloat(str1)
  var num2 = str2 == ''? 0 : parseFloat(str2)

  result = num1 + num2
  return result.toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  var posts = 0
  var comments = 0

  for(var i = 0; i < listOfPosts.length; i++) {
    if(listOfPosts[i].author === authorName)
      posts++;

    if(listOfPosts[i].comments != undefined)
      for(var j = 0; j < listOfPosts[i].comments.length; j++) {
        if(listOfPosts[i].comments[j].author == authorName)
          comments++
      }
  }
  return `Post:${posts},comments:${comments}`
};

const tickets=(people)=> {
  var m25 = 0
  var m50 = 0
  
  for(var i = 0; i < people.length; i++) {
    if(people[i] == 25)
      m25++;
    else if(people[i] == 50) {
      m50++;
      m25--;
    }
    else if(people[i] == 100) {
      if(m50 > 0) {
        m50--;
        m25--;
      }
      else
        m25 -= 3
    }
    if(m25 < 0 || m50 < 0) 
      return 'NO'
  }

  return 'YES'
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
